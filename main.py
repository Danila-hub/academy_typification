from typing import List


def sum_power_numbers(list_int: List[int]) -> int:
    """
    Computes the sum of powers of elements in a list of integers.

    Takes a list of numbers and calculates the sum of the numbers,
    where each number is calculated using the formula elem[i] ^ i.
    (The number from the list raised to the power of its index.)

    Arguments:
    list_int (list of int): List of integers for which the sum of powers will be computed.

    Returns:
    int: Sum of powers of elements in the list_int.

    int: 0 (zero) if the list_int contains any
    non-integer or negative elements.

    Examples:
    >>> sum_power_numbers([1, 2, 3, 4])
    76
    >>> sum_power_numbers([1,1,1,1,1,1])
    6
    >>> sum_power_numbers([1,2, -3])
    0
    """
    return (
        sum([value**index for index, value in enumerate(list_int)])
        if min(list_int) >= 0
        else 0
    )

